-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2022 at 07:05 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bingo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(2) NOT NULL,
  `admin_username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `admin_password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `admin_email` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_username`, `admin_password`, `admin_email`) VALUES
(1, 'Alhasan', 'c8a', 'alhasan1997@hotmail.com'),
(10, 'helper', 'c8a', 'alhasan1997@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(2) NOT NULL,
  `category_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`) VALUES
(7, 'Mobiles'),
(8, 'Laptobs'),
(9, 'Tablets');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(5) NOT NULL,
  `product_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `product_category_id` int(2) NOT NULL,
  `product_price` double NOT NULL,
  `product_description` text COLLATE utf8_unicode_ci NOT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_category_id`, `product_price`, `product_description`, `product_image`) VALUES
(16, 'Toshipa', 8, 13000, 'TOSHIPA', '1647954502135f8600-a448-11eb-9e7e-c680378c0683.cf.jpg'),
(17, 'HP', 8, 8000, 'hp ss123- Editbook', '16479545671750.webp'),
(18, 'Apple', 8, 25000, 'MacBoock Pro', '1647954652A4GDK27VMnz6LtFDy9yzk.jpg'),
(19, 'OPPO', 7, 4000, 'Oppo A74 5G', '1647954694download (1).jpg'),
(20, 'Samsung', 7, 3500, 'Samsung Galaxy A22', '1647955173download.jpg'),
(21, 'Samsung', 7, 23000, 'Samsung Galaxy S22 Plus', '1647955319GalaxyS22plus_MA_Thumb_1440x960.jpg'),
(22, 'HP', 8, 12000, 'hp 14-s Thinboock', '1647956105HP 14s.png'),
(23, 'Lenovo', 9, 5000, 'Lenovo tablet f2-54', '1647956263images (1).jpg'),
(24, 'Samsung', 7, 26000, 'samsung Galaxy Ultras 22-s', '1647956429images (2).jpg'),
(25, 'OPPO', 7, 6000, 'Oppo Reno 6 5G', '1647956522images (3).jpg'),
(26, 'Samsung', 9, 20000, 'Samsung Galaxy Fold', '1647956635images.jpg'),
(27, 'Apple', 7, 9000, 'iPhone  se-plus', '1647956726iphone-se-plus-specs-2.jpg'),
(28, 'Apple', 7, 16000, 'iPhone XS-Max', '1647956811iPhone-XS-Max.jpg'),
(29, 'itel', 7, 5000, 'itel P37', '1647956856itel-P37_________-________________________01.jpg'),
(31, 'OPPO', 7, 13000, 'Oppo Reno 7 Pro 5G', '1647957057opo71-600x600.jpg'),
(32, 'Dell', 8, 17000, 'Dell peripherals_laptop_latitude_3320', '1647957134peripherals_laptop_latitude_3320_gallery_1.jpg'),
(33, 'Samsung', 7, 24000, 'samsung-Galaxy-s22-5G', '1647957182samsung-galaxy-s22-5g-specs-1 (1) (1).jpg'),
(34, 'Tecno', 7, 3500, 'SPARK8T-blue', '1647957222SPARK8T-blue.png'),
(35, 'Apple', 8, 30000, 'Apple MacBoock', '1647957269u_10215001.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
